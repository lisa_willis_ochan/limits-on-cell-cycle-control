# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Accompanying scripts for the Cell Reports manuscript:
    
    Limits and constraints on mechanisms of cell-cycle regulation imposed by cell-size homeostasis measurements
    Lisa Willis, Henrik Jonsson, KC Huang.
	
* Written in Python 3.0 using Numpy and MatPlotLib packages.

### Who do I talk to? ###

* Contact: lisawillis@stanford.edu
