"""
@author: lisawillis
Python 3.0 using oackages Numpy and MatPlotLib.
Accompanying code for the Cell Reports manuscript:
    
    Limits and constraints on mechanisms of cell-cycle regulation imposed by cell-size homeostasis measurements
    Lisa Willis, Henrik Jonsson, KC Huang.

This code simulates single-cell trajectories of two-phase G1/S master regulators. 
Please refer to the Model results section for a description of parameters. 
Simulations support analytical results found in Methods and Methods S1.

"""
import numpy as np
import matplotlib.pyplot as plt


#SIMULATION PARAMETERS 
num_generations = 10000 #number of generations
dt = 0.001 #time increment

#CELL PROLIFERATION PARAMETERS
tau = 0.5  #Fraction of cell cycle taken up by G1
lambdaC = 1.0 #Size dependendence of regulator production
sigma = 2.0 #Average fold-increase in cell size between birth and division
lambda_T = 0.98 #Size scaling of regulator's localization region
r_SG2M = 1.0  #Gene copy number effects 
G2M_control = "timer" #Either timer, adder or size

#NOISE PARAMETERS
zeta_CP1 = 0.03 #Noise in G1/S threshold density for G1/S progression, corresponding to the coefficient of variation in the density.
zeta_CP2 = 0.03 #Noise in G2/M control mechanism, corresponding to the coefficient of variation in G2/M control mechanism.
zeta_1 = 0.03 #Noise in regulator production over G1
zeta_2 = 0.03 #Noise in regulator production over S/G2/M


#################################################
#OTHER PARAMETERS THAT DETERMINE UNITS OR FOR CONVENIENCE OF NOTATION
crit_density = 1.0 #Threshold density of regulator in localization region that triggers G1/S (units: C S^-lambdaT)
kappa = np.log(sigma) #Regulator production rate (units: C T^-1 S^-lambdaC) over G1
gamma = np.log(sigma) #Exponential growth rate (units: T^-1)

sig1 =np.power(sigma, tau) #Average fold increase over G1
sig2 = np.power(sigma, 1-tau)  #Average fold increase over S/G2/M
if G2M_control == "timer": f2 = sig2  
elif G2M_control == "adder": f2 = 1.0
elif G2M_control == "sizer": f2 = 0.0
else:
   print("Incorrect ")
   quit()
   
#############################################
#ANALYTICALLY PREDICTED VALUES OF CELL SIZE (S) AND REGULATOR (C) AT G1/S
S_G1S = np.power(kappa/gamma/crit_density*(r_SG2M/sigma*(np.power(sigma, (1-tau)*lambdaC) -1) + 1 - np.power(sigma, -tau*lambdaC))/lambdaC, 1.0/(lambda_T-lambdaC))
C_G1S = crit_density*np.power(S_G1S, lambda_T)
print("Predicted steady state size and regulator level at G1/S:", S_G1S, C_G1S)

mu_del1=  kappa/gamma* np.power(S_G1S/sig1, lambdaC)/lambdaC *( np.power(sig1, lambdaC) -1) #average change in C over G1/S
mu_del2= r_SG2M*kappa/gamma* np.power(S_G1S, lambdaC)/lambdaC *( np.power(sig2, lambdaC) -1) #average change in C over S/G2/M
mean_G2M_size = sig2 * S_G1S #AVERAGE SIZE AT G2/M

#INITIALIZE VARIABLES
size_trajectories = []
time_trajectories = []
acc_trajectories = []
T = 0 #INITIATE TIME
S = S_G1S #INITIATE CELL SIZE AT PREDICTED STEADY STATE VALUE

for generation in range(num_generations):
   #SIMULATE S/G2/M
   C = 0  #regulator has level 0 immediately after G1/S
   size_trajectories.append(S)
   time_trajectories.append(T)
   acc_trajectories.append(C)
   tau_temp = 0 
    
   ran_d = np.random.normal(0, zeta_2)
   ran = np.random.normal(0, zeta_CP2)
   C = C - ran_d * mu_del2 
   if C <0: C=0
   
   if G2M_control == "timer":
      while tau_temp < (1.0- tau)*(1.0 + ran):    
         S = S + gamma *S*dt
         C = C + r_SG2M* kappa * np.power(S,lambdaC)*dt
         tau_temp = tau_temp + dt
         T = T + dt  

   if G2M_control == "adder":
      Si = S
      while S- Si< mean_G2M_size*(1.0 - np.power(sigma, tau-1.0))*(1.0+ran):
         S = S + gamma *S*dt
         C = C + r_SG2M* kappa * np.power(S,lambdaC)*dt
         T = T + dt  

   if G2M_control == "sizer":
      while S < mean_G2M_size*(1.0 +ran): 
         S = S + gamma *S*dt
         C = C + r_SG2M* kappa * np.power(S,lambdaC)*dt
         T = T + dt           
         
   #SIZES AT G2/M OR DIVISION 
   size_trajectories.append(S)
   time_trajectories.append(T)
   acc_trajectories.append(C)
    
   S = S/sigma  #CELL DIVIDES, ONLY 1 DAUGHTER IS RETAINED  
   C = C/sigma

   #SIZES AT BIRTH
   size_trajectories.append(S)
   time_trajectories.append(T)
   acc_trajectories.append(C)
   
   #SIMULATE G1
   ran_d = np.random.normal(0, zeta_1)
   ran = np.random.normal(0, zeta_CP1)
   C = C - ran_d * mu_del1 
   if C <0: C=0
   while C/np.power(S, lambda_T) < crit_density*(1 + ran):     
      S = S + gamma *S*dt
      C = C + kappa * np.power(S,lambdaC)*dt
      T = T + dt  
   #SIZES AT G1/S      
   size_trajectories.append(S)
   time_trajectories.append(T)
   acc_trajectories.append(C)


#STATISTICS FROM ANALYTICAL RESULTS VS. SIMULATIONS. REFER TO METHODS AND METHODS S1.    
COV_G1S = np.std(size_trajectories[400::4])/np.mean(size_trajectories[400::4]) #COEFFICIENT OF VARIATION OF G1/S SIZE. IGNORE FIRST 100 GENERATIONS THAT MAY BE AFFECTED BY INITIAL CONDITIONS.
COV_G2M = np.std(size_trajectories[401::4])/np.mean(size_trajectories[401::4]) #COEFFICIENT OF VARIATION OF G2/M SIZE
beta = 1.0 - f2/sig2 + f2/sig2*np.log(f2) if f2!=0 else 1.0
eta2 = zeta_CP2/COV_G1S

f11=(r_SG2M/sigma*np.power(f2/sig2, 2.0)/(np.power(f2/sig2, 2.0) + np.power(beta*eta2, 2.0))  +  (np.power(sig1,-lambdaC) -r_SG2M/sigma*np.power(sig2,lambdaC)) *f2/sig2)/(1-lambda_T/lambdaC*(1 - np.power(sig1, -lambdaC) + r_SG2M/sigma*(np.power(sig2, lambdaC)-1)))
print("Analytically predicted slope between consecutive G2/Ms: ", f11)
print("Simulated slope between consecutive G2/Ms :", np.polyfit(np.array(size_trajectories)[101:-4:4], np.array(size_trajectories)[105::4], 1)[0]) 


f22=(r_SG2M/sigma +  (np.power(sig1,-lambdaC) -r_SG2M/sigma*np.power(sig2,lambdaC)) *f2/sig2)/(1-lambda_T/lambdaC*(1 - np.power(sig1, -lambdaC) + r_SG2M/sigma*(np.power(sig2, lambdaC)-1)))
print("Analytically predicted slope between consecutive G1/Ss: ", f22)
print("Simulated slope between consecutive G1/Ss :", np.polyfit(np.array(size_trajectories)[100:-4:4], np.array(size_trajectories)[104::4], 1)[0]) 


#PLOT SINGLE CELL TRAJECTORIES AT STEADY STATE
f, axarr = plt.subplots(2, sharex=True, figsize=(9,2.6))
end_p =302 #NUMBER OF GENERATIONS TO PLOT X 4.
pointSize =12
thickn =  0.5
#PLOT CELL SIZE AT G1/S BEFORE DEGRATION, G1/S AFTER DEGRADATION, AND G2/M AND BIRTH
axarr[0].plot(np.array(time_trajectories)[-end_p:], np.array(size_trajectories)[-end_p:], marker = None, linewidth = thickn, color = 'gold')
axarr[0].scatter(np.array(time_trajectories)[-end_p:][0::4], np.array(size_trajectories)[-end_p:][0::4], marker = 'o',  s=pointSize, color = 'gold')
axarr[0].scatter(np.array(time_trajectories)[-end_p:][1::4], np.array(size_trajectories)[-end_p:][1::4], marker = 'D',  s=pointSize, color = 'gold')
axarr[0].scatter(np.array(time_trajectories)[-end_p:][2::4], np.array(size_trajectories)[-end_p:][2::4], marker = 's',  s=pointSize, color = 'gold')
axarr[0].scatter(np.array(time_trajectories)[-end_p:][3::4], np.array(size_trajectories)[-end_p:][3::4], marker = 'o',  s=pointSize, color = 'gold')
#PLOT REGULATOR LEVEL AT G1/S BEFORE DEGRATION, G1/S AFTER DEGRADATION, AND G2/M AND BIRTH
axarr[1].plot(np.array(time_trajectories)[-end_p:], np.array(acc_trajectories)[-end_p:], marker = None, linewidth = thickn, color = 'lightseagreen')
axarr[1].scatter(np.array(time_trajectories)[-end_p:][0::4], np.array(acc_trajectories)[-end_p:][0::4], marker = 'o',  s=pointSize, color = 'lightseagreen')
axarr[1].scatter(np.array(time_trajectories)[-end_p:][1::4], np.array(acc_trajectories)[-end_p:][1::4], marker = 'D',  s=pointSize, color = 'lightseagreen')
axarr[1].scatter(np.array(time_trajectories)[-end_p:][2::4], np.array(acc_trajectories)[-end_p:][2::4], marker = 's',  s=pointSize, color = 'lightseagreen')
axarr[1].scatter(np.array(time_trajectories)[-end_p:][3::4], np.array(acc_trajectories)[-end_p:][3::4], marker = 'o',  s=pointSize, color = 'lightseagreen')

plt.xticks([])
plt.tight_layout()  
plt.savefig('MASTER_REGULATOR_single_cell_trajectories.pdf', format = 'pdf', dpi = 200)

